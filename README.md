## Simple test of graphicsHostLibrary

Originally written to walk W8LSS through the general use of the
graphicsHostLibrary, this project cycles through a number of simple
graphics exercises: lines, rectangles, filled figures, TT text and
graph.

Project has two configurations within MPLAB-X, one for the
PIC24FV16KM202 and one for the PIC24FV32KA301.
