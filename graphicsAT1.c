/*! \file  graphicsAT1.c
 *
 *  \brief Simple exercise of graphics terminal by thermometer board
 *
 *
 *  \author jjmcd
 *  \date July 20, 2015, 8:29 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

#define SCREENDELAY 1000
#define TTDELAY 500

/* Used to get pretty colors of lines and rectangles */
unsigned int colors[] = {
  RED, PINK, ROYALBLUE, FORESTGREEN, SPRINGGREEN, PERU, BURLYWOOD
  };

/*! main - Simple exercise of graphics terminal */
int main(void)
{
  int i;

  /* Initialize stuff */
  serialInitialize(0);
  TFTinit(TFTLANDSCAPE);

  /* Opening display */
  TFTsetBackColorX(BLACK);
  TFTsetColorX(WHITE);
  TFTsetFont(FONTDJS);
  TFTclear();
  putString(120, 100, 8, "Simple Test");
  delay(1000);

  /* This will go on for a long time */
  while (1)
    {
      /***********************************************************************/
      /* Draw a bunch of lines                                               */
      /***********************************************************************/
      TFTsetColorX(WHITE);
      TFTsetBackColorX(BLACK);
      TFTsetFont(FONTDJS);
      TFTclear();
      putString(140,10,8,"Lines");
      for ( i=0; i<6; i++ )
        {
          TFTsetColorX(colors[i]);
          TFTline(10,20+35*i,310,220-35*i);
        }
      delay(SCREENDELAY);

      /***********************************************************************/
      /* Now draw a bunch of rectangles                                      */
      /***********************************************************************/
      TFTsetColorX(WHITE);
      TFTclear();
      putString(130,10,8,"Rectangles");
      for ( i=0; i<6; i++ )
        {
          TFTsetColorX(colors[i]);
          TFTrect(10+20*i,50+15*i,200+20*i,120+15*i);
        }
      delay(SCREENDELAY);

      /***********************************************************************/
      /* Draw filled rectangles, circles                                     */
      /***********************************************************************/
      TFTsetColorX(WHITE);
      TFTclear();
      putString(100,10,8,"Filled Shapes");
      TFTsetColorX(DEEPPINK);
      TFTfillRect(10,50,200,100);
      TFTsetColorX(MEDIUMORCHID);
      TFTfillCircle(270,80,40);
      TFTsetColorX(GOLD);
      TFTfilledRoundRect(130,170,295,215);
      delay(SCREENDELAY);

      /***********************************************************************/
      /* Do a little dancing around with TT text                             */
      /***********************************************************************/
      TFTsetColorX(WHITE);
      TFTclear();
      putString(150,10,8,"Text");
      TFTsetColorX(CYAN);
      TFTputsTT("\033[4;1HA little bit of text\r\nwith a newline");
      TFTsetColorX(LIMEGREEN);
      TFTputsTT("\033[4;30HNumbers");
      TFTsetColorX(YELLOW);
      TFTprintDec(1234,2,240,70);
      TFTprintDec(5678,4,240,85);
      TFTsetColorX(PERU);
      TFTputsTT("\033[10;1HAnd a demo of erase to beginning and\r\nend of line\r\n");
      delay(SCREENDELAY);
      TFTputsTT("\033[12;1H**************************************");
      delay(TTDELAY);
      TFTputsTT("\033[12;20H beginning ");
      delay(TTDELAY);
      TFTputsTT("\033[12;19H\033[1K");
      delay(TTDELAY);
      TFTputsTT("\033[12;1H**************************************");
      delay(TTDELAY);
      TFTputsTT("\033[12;16H end ");
      delay(TTDELAY);
      TFTputsTT("\033[12;20H\033[0K");
      delay(TTDELAY);
      delay(SCREENDELAY);

      /***********************************************************************/
      /* Finally, show how the graph function works                          */
      /***********************************************************************/
      TFTGinit();
      TFTGtitle("Test Graph");
      TFTGrange(0,300,0,100);
      TFTGexpose();
      for(i=0; i<277; i++ )
        {
          TFTGaddPoint( (long)((i/2)*(i/2))/((long)(105)) );
        }
      delay(SCREENDELAY);

    }

  return 0;
}
