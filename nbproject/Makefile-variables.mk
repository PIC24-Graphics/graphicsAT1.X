#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=graphicsAT1.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/graphicsAT1.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=graphicsat1.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/graphicsat1.x.tar
# KM202 configuration
CND_ARTIFACT_DIR_KM202=dist/KM202/production
CND_ARTIFACT_NAME_KM202=graphicsAT1.X.production.hex
CND_ARTIFACT_PATH_KM202=dist/KM202/production/graphicsAT1.X.production.hex
CND_PACKAGE_DIR_KM202=${CND_DISTDIR}/KM202/package
CND_PACKAGE_NAME_KM202=graphicsat1.x.tar
CND_PACKAGE_PATH_KM202=${CND_DISTDIR}/KM202/package/graphicsat1.x.tar
# KA301 configuration
CND_ARTIFACT_DIR_KA301=dist/KA301/production
CND_ARTIFACT_NAME_KA301=graphicsAT1.X.production.hex
CND_ARTIFACT_PATH_KA301=dist/KA301/production/graphicsAT1.X.production.hex
CND_PACKAGE_DIR_KA301=${CND_DISTDIR}/KA301/package
CND_PACKAGE_NAME_KA301=graphicsat1.x.tar
CND_PACKAGE_PATH_KA301=${CND_DISTDIR}/KA301/package/graphicsat1.x.tar
